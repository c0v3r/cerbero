package stepsDefinition;



import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(glue = "stepsDefinition",
                 features= "src/test/resources/features/"
                )
public class TestRunner {
}
