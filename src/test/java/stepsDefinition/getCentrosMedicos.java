package stepsDefinition;


import com.jayway.restassured.RestAssured;
import com.jayway.restassured.internal.RestAssuredResponseImpl;
import cucumber.api.DataTable;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.seleniumhq.jetty9.http.HttpStatus;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import static com.jayway.restassured.path.json.JsonPath.from;
import static org.assertj.core.api.Assertions.assertThat;



public class getCentrosMedicos{

    private String usuarioToken = "retia";

    private String passwordToken = "redsanitaria";

    private RestAssuredResponseImpl responseList;

    private String accessToken;

    @Before
    public static void setup() {

        //Base Port
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8080);
        }
        else{
            RestAssured.port = Integer.valueOf(port);
        }

        //Base Path
        String basePath = System.getProperty("server.base");
        if(basePath==null){
            basePath = "/api/v1/";
        }
        RestAssured.basePath = basePath;

        //Base Host
        String baseHost = System.getProperty("server.host");
        if(baseHost==null){
            baseHost = "https://api-stg.redsanitaria.com";
        }
        RestAssured.baseURI = baseHost;

    }

    @Given("Ingreso con usuario '(.*)' y clave '(.*)'")
    public void getAuthToken(String userName, String userPassword){
        accessToken = RestAssured.given().auth()
                .basic(usuarioToken,passwordToken)
                .formParam("grant_type", "password")
                .formParam("username", userName)
                .formParam("password", userPassword)
                .when()
                .post("https://api-stg.redsanitaria.com/oauth/token")
                .then()
                .assertThat()
                .statusCode(HttpStatus.OK_200)
                .extract()
                .path("access_token");
    }

    @When("cuando consulto el listado de centros-medicos")
    public void getCentroMedicos(){
       responseList = (RestAssuredResponseImpl)
                RestAssured
                        .given()
                        .auth()
                        .oauth2(accessToken)
                        .when()
                        .get("/centros-medicos")
                        .then()
                        .extract()
                        .response();
    }

    @Then("valido los siguentes datos del centro medico:")
    public void validateMedicalList(DataTable arg){
        List<Map<String, String>> list = arg.asMaps(String.class, String.class);
        assertThat(responseList.getStatusCode()).isEqualTo(200);
        assertThat(responseList.getContentType().contains("application/json"));
        ArrayList<Map<String,String>> jsonAsArrayList = from(responseList.asString()).get("");

        assertThat(responseList.asString().contains(list.get(0).get("idCentro"))).isTrue();
        assertThat(jsonAsArrayList.get(0).get("denomCentro").contains(list.get(0).get("denomCentro")));
    }

    @When("intento acceder al listado de centros medicos sin credenciales")
    public void getCentrosMedicosNoToken() {
        responseList = (RestAssuredResponseImpl)
                RestAssured
                        .given()
                        .when()
                        .get("/centros-medicos")
                        .then()
                        .extract()
                        .response();
    }

    @Then("valido el error al querer acceder al listado de centros medicos sin credenciales")
    public void validateError(){

        assertThat(responseList.getStatusCode()).isEqualTo(401);
    }
}
