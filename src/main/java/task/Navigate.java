package task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Open;
import net.thucydides.core.annotations.Step;
import ui.LoginPage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class Navigate implements Task {
    LoginPage loginPage;

    public static Navigate loginPage() {
        return instrumented(Navigate.class);
    }

  //  public static Navigate withAppOption(String option) {return }

    //@Step("starts a google search")
    public <T extends Actor> void performAs(T t) {
        t.attemptsTo(Open
                .browserOn()
                .the(loginPage));
    }
}
