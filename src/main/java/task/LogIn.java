package task;

import net.serenitybdd.screenplay.Actor;
import net.serenitybdd.screenplay.Task;
import net.serenitybdd.screenplay.actions.Click;
import net.serenitybdd.screenplay.actions.Enter;
import net.thucydides.core.annotations.Step;
import ui.LoginPage;

import static net.serenitybdd.screenplay.Tasks.instrumented;

public class LogIn implements Task {
    private final String email;
    private final String password;

    public LogIn(String email, String password){
        this.email = email;
        this.password = password;
    }

    @Step("{0} Login with email '#email' and password '#password'")
    public <T extends Actor> void performAs(T actor) {
        actor.attemptsTo(Enter.theValue(email).into(LoginPage.EMAIL_TEXTFIELD));
        actor.attemptsTo(Enter.theValue(password).into(LoginPage.PASSWORD_TEXTFIELD));
        actor.attemptsTo(Click.on(LoginPage.SUBMIT_BUTTON));
    }

    public static LogIn withCredentials(String email, String password) {
        return instrumented(LogIn.class, email, password);
    }
}
