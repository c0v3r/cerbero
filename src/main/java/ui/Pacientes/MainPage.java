package ui.Pacientes;

import net.serenitybdd.screenplay.targets.Target;
import org.openqa.selenium.By;

public class MainPage {

    public static final Target PATIENT_MAIN_PAGE_LABEL = Target.the("Patients main page title")
            .located(By.cssSelector("h6.hidden"));

}
