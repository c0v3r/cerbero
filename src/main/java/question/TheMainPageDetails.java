package question;

import net.serenitybdd.screenplay.Question;
import net.serenitybdd.screenplay.questions.targets.TheTarget;
import net.serenitybdd.screenplay.targets.Target;
import ui.HomePage;
import ui.RRHH.MainPage;

public class TheMainPageDetails {

    public static Question<String> title(String page) {
        switch (page) {
            case "Recepción":
                return TheTarget.textOf(ui.Administracion.MainPage.RECEPTION_MAIN_PAGE_LABEL);
            case "Pacientes":
                return TheTarget.textOf(ui.Pacientes.MainPage.PATIENT_MAIN_PAGE_LABEL);
            case "Personal":
                return  TheTarget.textOf(MainPage.RRHH_MAIN_PAGE_LABEL);
        }
        return null;
    }
}
